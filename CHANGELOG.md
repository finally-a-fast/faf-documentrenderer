[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog |  Documentrenderer
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2020-12-08
### Added
- Project Files @StefanBrandenburger
- DocumentRenderer.php, PdfRenderer.php and DocumentHandlerInterface.php @StefanBrandenburger

[Unreleased]: https://gitlab.com/finally-a-fast/faf-documentrenderer/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/faf-documentrenderer/-/tree/v0.1.0-alpha
