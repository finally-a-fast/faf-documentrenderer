<?php
/**
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/faf-documentrenderer/license MIT
 * @link https://www.finally-a-fast.com/packages/faf-documentrenderer
 * @see https://www.finally-a-fast.com/packages/faf-documentrenderer/docs Documentation of faf-documentrenderer
 * @since File available since Release 1.0.0
 */


namespace Faf\DocumentRenderer\Interfaces;


use Faf\DocumentRenderer\DocumentRenderer;

/**
 * Interface DocumentHandlerInterface
 *
 * @package Faf\DocumentRenderer\Interfaces
 */
interface DocumentHandlerInterface
{
    /**
     * @param DocumentRenderer $renderer
     * @param                  $parser
     *
     * @return bool
     */
    public function saveToDb(DocumentRenderer $renderer, $parser): bool;

    /**
     * @param DocumentRenderer $renderer
     * @param                  $parser
     *
     * @return bool
     */
    public function saveToFileSystem(DocumentRenderer $renderer, $parser): bool;

    /**
     * @param DocumentRenderer $renderer
     *
     * @return mixed
     */
    public function setData(DocumentRenderer $renderer): void;

    /**
     * You need to return something similar to this:
     * ```php
     * return [
     *    'header'  => [
     *      0 => [
     *          'name'          => 'Header',
     *          'type'          => 'header',
     *          'sort'          => '1',
     *          'width'         => null,
     *          'height'        => null,
     *          'float'         => 'left',
     *          'align'         => 'left',
     *          'valign'        => 'top',
     *          'position'      => 'static',
     *          'x'             => null,
     *          'y'             => null,
     *          'orientation_x' => 'left',
     *          'orientation_y' => 'top',
     *          'margin_top'    => 0,
     *          'margin_right'  => 0,
     *          'margin_bottom' => 0,
     *          'margin_left'   => 0,
     *          'settings' => '{"backTop":"50mm"}',
     *          'content'  => 'Hello World'
     *      ]),
     *   ],
     *   'footer'  => [...],
     *   'content' => [...],
     * ];
     * ```
     *
     * @return array[]
     */
    public function getElements(): array;
}
