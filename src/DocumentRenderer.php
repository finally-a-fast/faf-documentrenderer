<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/faf-documentrenderer/license MIT
 * @link https://www.finally-a-fast.com/packages/faf-documentrenderer
 * @see https://www.finally-a-fast.com/packages/faf-documentrenderer/docs Documentation of faf-documentrenderer
 * @since File available since Release 1.0.0
 */

namespace Faf\DocumentRenderer;

use Faf\DocumentRenderer\Interfaces\DocumentHandlerInterface;
use Faf\TemplateEngine\Parser;
use RuntimeException;


/**
 * Class DocumentRenderer
 *
 * @package Faf\DocumentRenderer
 */
abstract class DocumentRenderer
{
//    public const PARSER_TYPE = Parser::TYPE_PAGE;

    /** @var object|false */
    public $content = null;

    /** @var string|null */
    public ?string $fileName = null;

    /** @var array */
    public array $data = [];

    /** @var array  */
    public array $fontFiles = [];

    /** @var string|null  */
    public ?string $default_font = null;

    public string $title;
    public string $orientation = 'portrait';
    public float $width;
    public float $height;
    public float $margin_left;
    public float $margin_top;
    public float $margin_right;
    public float $margin_bottom;

    public string $creator = '';
    public string $author = '';


    /** @var Parser */
    protected $parser;

    /** @var DocumentHandlerInterface */
    protected DocumentHandlerInterface $documentHandler;

    /**
     * @return void
     */
    abstract public function renderDocument(): void;

    public function __construct(DocumentHandlerInterface $handler, $parser, array $data = [])
    {
        $this->documentHandler = $handler;
        $this->parser          = $parser;
        $this->data            = $data;
    }

    /**
     * @return bool
     */
    public function validateContent(): bool
    {
        return $this->content !== false;
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function save(): bool
    {
        if ($this->content === null) {
            $this->renderDocument();
        }

        if (!$this->validateContent()) {
            return false;
        }

        if (!$this->documentHandler->saveToFileSystem($this, $this->parser)) {
            throw new RuntimeException('Document can not be saved to file system');
        }

        if (!$this->documentHandler->saveToDb($this, $this->parser)) {
            throw new RuntimeException('Document can not be saved to database');
        }

        return true;
    }
}
