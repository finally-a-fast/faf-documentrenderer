<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/faf-documentrenderer/license MIT
 * @link https://www.finally-a-fast.com/packages/faf-documentrenderer
 * @see https://www.finally-a-fast.com/packages/faf-documentrenderer/docs Documentation of faf-documentrenderer
 * @since File available since Release 1.0.0
 */

namespace Faf\DocumentRenderer;

use Spipu\Html2Pdf\{Exception\Html2PdfException, Html2Pdf};

/**
 * Class PdfRenderer
 *
 * @package Faf\DocumentRenderer
 */
class PdfRenderer extends DocumentRenderer
{
    /** @var Html2Pdf|false */
    public $content;

    public ?string $css = null;

    /**
     * @throws \Exception
     */
    public function renderDocument(): void
    {
        $this->documentHandler->setData($this);

        /* @todo
        lang = 'de'
        unicode = true
        'UTF-8'
        */
        try {
            $pdf = new Html2Pdf(
                ($this->orientation === 'landscape' ? 'L' : 'P'), [$this->height * 10, $this->width * 10], 'de', true, 'UTF-8', [
                $this->margin_left * 10,
                $this->margin_top * 10,
                $this->margin_right * 10,
                $this->margin_bottom * 10
            ]
            );

            //todo
            $pdf->pdf->SetCreator($this->creator);
            $pdf->pdf->SetAuthor($this->author);
            //$pdf->pdf->SetTitle($this->title);
            //$pdf->pdf->setPrintHeader(false);
            //$pdf->pdf->setPrintFooter(false);

            $pdf->pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
            //$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            $pdf->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // setSignature($signing_cert='', $private_key='', $private_key_password='', $extracerts='', $cert_type=2, $info=array(), $approval='') {
            // to create self-signed signature: openssl req -x509 -nodes -days 365000 -newkey rsa:1024 -keyout tcpdf.crt -out tcpdf.crt
            // to export crt to p12: openssl pkcs12 -export -in tcpdf.crt -out tcpdf.p12
            // to convert pfx certificate to pem: openssl
            //     OpenSSL> pkcs12 -in <cert.pfx> -out <cert.crt> -nodes
            //$pdf->SetAutoPageBreak(true, );

            $pdfContent               = '';
            $backTop                  = '0mm';
            $backBottom               = '0mm';
            $documentTemplateElements = $this->documentHandler->getElements();
            $headerCount              = count($documentTemplateElements['header'] ?? []);
            $footerCount              = count($documentTemplateElements['footer'] ?? []);

            if ($headerCount > 1) {
                throw new \Exception('Only one header element is allowed!');
            }

            if ($footerCount > 1) {
                throw new \Exception('Only one footer element is allowed!');
            }

            if ($headerCount > 0) {
                $pdfContent .= '<page_header>' . $this->renderElements($documentTemplateElements, 'header') . '</page_header>';
                $backTop = $backBottom = json_decode($documentTemplateElements['header'][0]['settings'], true, 512, JSON_THROW_ON_ERROR)['backTop'] ?? $backTop;
            }

            if ($footerCount > 0) {
                $pdfContent .= '<page_footer>' . $this->renderElements($documentTemplateElements, 'footer') . '</page_footer>';
                $backBottom = json_decode($documentTemplateElements['footer'][0]['settings'], true, 512, JSON_THROW_ON_ERROR)['backBottom'] ?? $backBottom;
            }

            $pdfContent .= $this->renderElements($documentTemplateElements, 'content');

            $css  = 'h1 {font-size:' . (PDF_FONT_SIZE_MAIN * 4.2) . 'pt;}';
            $css .= 'h2 {font-size:' . (PDF_FONT_SIZE_MAIN * 3.56) . 'pt;}';
            $css .= 'h3 {font-size:' . (PDF_FONT_SIZE_MAIN * 2.92) . 'pt;}';
            $css .= 'h4 {font-size:' . (PDF_FONT_SIZE_MAIN * 2.28) . 'pt;}';
            $css .= 'h5 {font-size:' . (PDF_FONT_SIZE_MAIN * 1.64) . 'pt;}';
            $css .= 'h6 {font-size:' . (PDF_FONT_SIZE_MAIN) . 'pt;}';
            $css .= $this->css ?? '';

            foreach ($this->fontFiles as $fontFile) {
                if (file_exists($fontFile)) {
                    \TCPDF_FONTS::addTTFfont($fontFile);
                }
            }

            if (!empty($this->default_font)) {
                $pdf->setDefaultFont($this->default_font);
            }

            $pdfHtml = '<style type="text/css"><!--' . $css . ' --></style>' . '<page backtop="' . $backTop . '" backbottom="' . $backBottom . '" backleft="0" backright="0">' . $pdfContent . '</page>';

            $pdf->setTestTdInOnePage(true);
            $pdf->WriteHTML($pdfHtml);
        } catch (\Exception $e) {
            $pdf->clean();

            throw $e;
        }

        $this->content = $pdf;
    }

    /**
     * @param array  $elements
     * @param string $type
     *
     * @return string
     * @throws \Exception
     */
    protected function renderElements(array $elements, string $type = 'content'): string
    {
        $pdfContent = '';

        $this->parser->setData($this->data);

        if (count($elements[$type] ?? []) > 0) {
            foreach ($elements[$type] as $element) {
                $options = [
                    'text-align'        => $element['align'],
                    'float'             => $element['float'],
                    'clear'             => 'both',
                    'margin'            => ($element['margin_top'] * 10) . 'mm ' . ($element['margin_right'] * 10) . 'mm ' . ($element['margin_bottom'] * 10) . 'mm ' . ($element['margin_left'] * 10) . 'mm',
                    'page-break-after'  => 'avoid',
                    'page-break-before' => 'avoid',
                ];

                $tagName = 'span';

                if (!empty($element['height'])) {
                    $tagName           = 'div';
                    $options['height'] = ($element['height'] * 10) . 'mm';
                }

                if (!empty($element['width'])) {
                    $tagName          = 'div';
                    $options['width'] = ($element['width'] * 10) . 'mm';
                } else {
                    $options['width'] = '100%';
                }

                if ($element['position'] === 'absolute') {
                    $tagName                            = 'div';
                    $options['position']                = 'absolute';
                    $options[$element['orientation_y']] = ($element['y'] * 10) . 'mm';
                    $options[$element['orientation_x']] = ($element['x'] * 10) . 'mm';
                }

                $pdfContent .= "<$tagName";

                foreach ($options as $attr => $value) {
                    $pdfContent .= " $attr=\"$value\"";
                }

                $pdfContent .= '>';
                $pdfContent .= $this->parser->parse($element['content']);
                $pdfContent .= "</$tagName>";

//                $pdfContent .= Html::tag($tagName, $content, [
//                    'style' => $options,
//                     $tagName
//                ]);
            }
        }

        return $pdfContent;
    }
}
